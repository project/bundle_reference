CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Adds a new bundle reference field type, widget and formatter.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/bundle_reference

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/bundle_reference


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Bundle Reference module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > [Content type to
       create or edit] and add a new field.
    3. There is now a "Bundle Reference" field in the dropdown.
    4. Chose which bundles to reference. Save Settings.


MAINTAINERS
-----------

 * Marcin Grabias (Graber) - https://www.drupal.org/u/graber
